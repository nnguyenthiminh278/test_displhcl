
import numpy as np
import math
import sys


# -----------------------------------------------------------------------------------------------------------
# ----- This script is used to get coordinates of normal modes of ES from Gaussian (.fchk file) -------------
# -------------- and convert to the matrix 1 x 3N where N is number of atoms --------------------------------
# - type: python3 Get_nm_3.py HCl_ES.fchk nm_ES.txt ---------------------------------------------------------
# -------------- Find "Vib-Modes" in .fhck file -------------------------------------------------------------

def getrawdata(infile):
    items = 0
    countlines = 0
    hessian = []                                            # save found data in this list
    f = open(infile, 'r')                                   # read input file
    found = False
    for line in f:
        line = line.rstrip('\n')                            # only the rightmost \n will be removed, leaving all the other untouched
        if found == True:
            data = list(filter(None, line.split(' ')))
            for n in range(0, len(data)):
                hessian += [float(data[n])]
                countlines += 1

        if "Vib-Modes" in line:
            found = True
            data = list(filter(None, line.split(' ')))
            items = int(data[data.index("N=")+1])

        if countlines == items:
            found = False

    f.close()

    return hessian

# ------------ Convert the obtained data to a matrix ------------------------------------------

def savecoord(rawdata, outfile):
    m = len(rawdata)
    #n = m//3 # n=m/number of modes
    M = np.reshape(rawdata,(1, m)) # save the extracted coords in matrix
    #print(M)
    # To get the othornormal mass-weighted matrix
    mass_Cl = math.sqrt(34.9688527* 1822.88848) # mass of Cl in au: 1 atomic mass unit has the mass of approximately 1822 electrons
    mass_H = math.sqrt(1.00782504*1822.88848)
    M[:,:3] = M[:,:3]*mass_Cl  # get MW matrix
    M[:,3:] = M[:,3:]*mass_H 
    M_norm = np.linalg.norm(M, axis=-1, keepdims=True)
    M = M / M_norm 
    print("After normalization:") 
    print(M)  
    np.savetxt(outfile, M, delimiter=" ")


# To check the othorgonalization: the off-diagonals of matrix A should be 0 and diagonal terms = 1 :
    N = M.T # calculate transpose
    A = M@N 
    print("Check normalization:") 
    print(A)
#----------------------------------------------
   
if __name__ == "__main__":
    infile = sys.argv[1]
    outfile = sys.argv[2]
    rawdata = getrawdata(infile)
    savecoord(rawdata, outfile)
