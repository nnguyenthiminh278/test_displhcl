
import numpy as np
import math
import sys

# -----------------------------------------------------------------------------------------------------------
# ----- This script is used to get coordinates of normal modes of ES from Midascpp (FinalVib.mmol file) -------------
# -------------- and convert to the matrix 1 x 3N where N is number of atoms --------------------------------
# - type: python3 Get_nm_3_Midas.py FinalVib.mmol nm_Midas.txt ---------------------------------------------------------
# -------------- Find "#2COORDINATE" in .fhck file -------------------------------------------------------------

def get_wj(infile, n):
    wj= []
    f = open(infile, 'r')
    line = f.readline()
    while line:
        line = line.rstrip('\n')
        if "#2COORDINATE" in line:
            for _ in range(n):
                for w in f.readline().split(" "):
                    s = w.strip()
                    try:
                        wj.append(float(s))
                    except ValueError:
                        pass
        line = f.readline()
    f.close()
    return np.array(wj)

get_wj('FinalVib.mmol', 3)
    
# ------------ Convert the obtained data to a matrix ------------------------------------------

def savecoord(rawdata, outfile):
    m = len(rawdata)
    #n = m//3
    M = np.reshape(rawdata,(3, m)) # save the extracted coords in matrix modes x 3*atoms
    mass_Cl = math.sqrt(34.9688527)
    mass_H = math.sqrt(1.00782504)
    M[:,:3] = M[:,:3]*mass_Cl  # Normalize the matrix M by mult the coords of oxygen by square root of its mass 
    M[:,3:] = M[:,3:]*mass_H #Normalize    
    np.savetxt(outfile, M, delimiter=" ")


# To check the othorgonalization: the off-diagonals of matrix A should be 0 and diagonal terms = 1 :

    N = M.T # calculate transpose
    A = M@N 
    print(A)
#    np.savetxt(outfile, A, delimiter=" ")
#----------------------------------------------
   
if __name__ == "__main__":
    infile = sys.argv[1]
    outfile = sys.argv[2]
    rawdata = get_wj(infile,3)
    #rawdata = getrawdata(infile)
    savecoord(rawdata, outfile)
