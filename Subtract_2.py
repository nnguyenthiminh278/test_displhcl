import numpy as np
import math
import sys

# ---------------------------------------------------------------------------------------
# -------------- This script is used to get the difference of coordinates ---------------
# ------------------------------ between GS and ES --------------------------------------
# type: python3 Subtract_2.py displ.txt 
# ---------------------------------------------------------------------------------------
# Open a file
line1 = []
with open('GS.txt', 'r') as f_GS:
    for line in f_GS.readlines():
        for num in line.split(' '):
            line1.append(float(num))
    print("Coordinates of ground state: %s" %(line1))

line2 = []
with open('ES.txt', 'r') as f_ES:
    for line in f_ES.readlines():
        for num in line.split(' '):
            line2.append(float(num))
    print("Coordinates of excited state: %s" % (line2))

result=[]
result = [line1_i - line2_i for line1_i, line2_i in zip(line1, line2)]

# ------------ Save the obtained subtraction to raw matrix then normalize  -----------------------

def savecoord(result, outfile):
    m = len(result)
    mass_Cl = math.sqrt( 34.9688527* 1822.88848) # mass of Cl in au
    mass_H = math.sqrt(1.00782504 * 1822.88848) # mass of H in au
    M = np.reshape(result,(1, m)) #save to matric 1xm
    M[:,:3] = M[:,:3]*mass_Cl  # get MW matrix
    M[:,3:] = M[:,3:]*mass_H 
    np.savetxt(outfile, M, delimiter=" ")

# Close opend file
f_GS.close()
f_ES.close()

if __name__ == "__main__":
    outfile = sys.argv[1]
    savecoord(result, outfile)