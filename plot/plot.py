#############################################
### python3 abs_ex2.py file1.dat file2.dat ###
###                    BP86      CAM-B3LYP ###
##############################################
import os
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

file1=sys.argv[1]  # ES_point_prop1_Qx.mplot
file2=sys.argv[2]  # GS_point_prop2_Qx.mplot
#file3=sys.argv[3]  # EXP

x_arr1=np.loadtxt(file1,usecols=(0))
y_arr1=np.loadtxt(file1,usecols=(1))
x_arr2=np.loadtxt(file2,usecols=(0))
y_arr2=np.loadtxt(file2,usecols=(1))
#x_arr3=np.loadtxt(file3,usecols=(0))
#y_arr3=np.loadtxt(file3,usecols=(1))
fig = plt.figure()

sp = fig.add_subplot(1,1,1)
sp.plot(x_arr1,y_arr1,color='red',label='ES', linewidth=2)
sp.plot(x_arr2,y_arr2,color='black',label='GS ')
#sp.plot(x_arr3,y_arr3,color='black',label='EXP ')
#plt.axvline(x=186.35, color='grey', linewidth=2, label='Vertical excitation')
#sp.plot(x_arr,y_arr)
sp.set_xlabel("Displ")
sp.set_ylabel("E")
sp.legend(loc='upper right')
#sp.set_title("Absorption spectrum", loc='center')

def annot_min1(x,y, ax=None):
    xmin = x[np.argmin(y)]
    ymin = y.min()
    text= "x={:.3f}, y={:.3f}".format(xmin, ymin)
    if not ax:
        ax=plt.gca()
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    arrowprops=dict(arrowstyle="->",connectionstyle="angle,angleA=0,angleB=60")
    kw = dict(xycoords='data',textcoords="axes fraction",
              arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
    ax.annotate(text, xy=(xmin, ymin), xytext=(0.54,0.56), **kw)

annot_min1(x_arr1,y_arr1)
def annot_min2(x,y, ax=None):
    xmin = x[np.argmin(y)]
    ymin = y.min()
    text= "x={:.3f}, y={:.3f}".format(xmin, ymin)
    if not ax:
        ax=plt.gca()
    bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
    arrowprops=dict(arrowstyle="->",connectionstyle="angle,angleA=0,angleB=60")
    kw = dict(xycoords='data',textcoords="axes fraction",
              arrowprops=arrowprops, bbox=bbox_props, ha="right", va="top")
    ax.annotate(text, xy=(xmin, ymin), xytext=(0.64,0.66), **kw)

annot_min2(x_arr2,y_arr2)

plt.savefig('HCl_pes.png', format='png')
plt.close()

##############################################

